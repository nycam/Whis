var nounous
$(function(){
  actualiser();
});

function recupererNounouBloques(){
  $.ajax({
    url:"/donnees/admin/nounous/bloquees",
    success:function(result){
      insererNounouBloques(result);
    }
  });
}
function insererNounouBloques(nounous){
  $('#datasNounouAttente').empty();
  nounous.forEach(function(nounou){
    $('#datasNounouAttente').append(creerDonnees(nounou, 0));
  })
  $('.debloquer').off('click');
  $('.debloquer').on('click', function(){
    debloquer($(this).attr('id'));
  });
  $('.supprimer').off('click');
  $('.supprimer').on('click', function(){
    supprimer($(this).attr('id'));
  });
}

function recupererNounou(){
  $.ajax({
    url:"/donnees/admin/nounous",
    success:function(result){
      insererNounou(result);
    }
  });
}

function insererNounou(nounous){
  $('#datasNounou').empty();
  nounous.forEach(function(nounou){
    $('#datasNounou').append(creerDonnees(nounou, 1));
  });
  $('.bloquer').off('click');
  $('.bloquer').on('click', function(){
    bloquer($(this).attr('id'));
  });
}
function recupererLangue(){

}
function creerDonnees(nounou, type){
  var langues = "";
  nounou["langue"].forEach(function(langue){
    langues+=langue+"/";
  });
  var buttons = "";
  if(type==0){
    buttons+="<div class='zoneBouton'><input id='"+nounou["id"]+"' type='button' class='supprimer btn btn-danger' value='Supprimer'><input type='button' id='"+nounou["id"]+"' class='debloquer btn btn-success' value='Débloquer'></div>"
  }else{
    buttons+="<div class='zoneBouton2'><input type='button' id='"+nounou["id"]+"' class='bloquer btn btn-danger' value='Bloquer'></div>";
  }
  let strNounou=
  "<div class='nounou' >"+
    "<div class='photo'>"+
      "<img class='photoNounou' src='"+nounou["photo"]+"'>"+
    "</div>"+
    "<div class='data'>"+
      "<div><b>Nom : </b>"+nounou["prenom"]+" "+nounou["nom"]+"</div>"+
      "<div><b>Portable : </b>"+nounou["portable"]+" <b>Ville : </b>"+nounou["ville"]+"</div>"+
      "<div><b>Langues : </b>"+langues+"</div>"+
      "<div><b>Presentation : </b>"+nounou["presentation"]+"</div>"+
    "</div>"+
    buttons+
  "</div>";
  return strNounou;
}
function bloquer(id){
  $.ajax({
    url:"/traitement/admin/nounou/"+id+"/bloquer",
    success:function(result){
      actualiser();
    }
  });
}

function debloquer(id){
  $.ajax({
    url:"/traitement/admin/nounou/"+id+"/debloquer",
    success:function(result){
      actualiser();
    }
  });
}

function supprimer(id){
  $.ajax({
    url:"/traitement/admin/nounou/"+id+"/supprimer",
    success:function(result){
      actualiser();
    }
  });
}
function actualiser(){
  recupererNounouBloques();
  recupererNounou();
}
