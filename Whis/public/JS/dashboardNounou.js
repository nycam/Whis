var datas = [];
var notifs = [];
var notifActuelle = 0;
var $calendar = null;
$(function(){
  $(window).resize(function(){
    $('#calendar').fullCalendar('option', 'height', $('#calendar').height());
    console.log($(window).width());
    if($(window).width()<767){
      $('#calendar').fullCalendar('changeView', 'agendaDay');
    }else{
      $('#calendar').fullCalendar('changeView', 'agendaWeek');
    }

  })
  $(document).keypress(function(e){
    if (e.which == 0){
      $('#overlay').css("display", "none");
      $("#Valider").off("click");
    }
  });
  $calendar = $("#calendar");
  $calendar.fullCalendar({
    defaultView: 'agendaWeek',
    height: $('#calendar').height(),
    locale: 'fr',
    dayClick: function(date){
      $('#overlay').css('display', 'block');
      $('#dateDebut').val(moment(date).format('YYYY-MM-D'));
      $('#timeDebut').val(moment(date).format("HH:mm"));
      $('#dateFin').val(moment(date).format('YYYY-MM-D'));
      $('#timeFin').val(moment(date).add(1, 'hours').format("HH:mm"));
      $("#Valider").on("click", ajouterDispo);
    },
    eventClick: function(calEvent, jsEvent, view){
      console.log(calEvent.title+" "+calEvent.id);
    }
  });
  if($(".suiv").length && $(".prec").length){
    $("#btnSuiv").on("click", nextNotification);
    $("#btnPrec").on("click", prevNotification);
  }
  accepterReservation();
  refuserReservation();
  actualiserDispo();
});

function actualiserDispo() {
	$.ajax({
		url: "/donnees/nounous/creneaux",
		success: (res) => {
      $('.carteNotif').empty();
      datas.length = 0;
      notifs.length = 0;
			res.forEach((dispo) => {
				afficherDispo(dispo)
			});
      if(notifs.length>0){
        creerNotification(notifs[0]);
      }
      $calendar.fullCalendar('removeEventSource', datas);
      $calendar.fullCalendar('addEventSource', datas);
		}
	});

}

function ajouterDispo(){
  let dateDebut = $("#dateDebut").val()+" "+$("#timeDebut").val();
  let dateFin = $("#dateFin").val()+" "+$("#timeFin").val();
  let recur = $("#recurrence").val();
  $.ajax({
    url:"/traitement/nounou/creneau/ajouter",
    type: "POST",
    data: "dateDebut="+dateDebut+"&dateFin="+dateFin+"&recur="+recur,
    success: (res)=>{
      actualiserDispo();
    }
  });
  $("#dateDebut").val("");
  $("#timeDebut").val("");
  $("#dateFin").val("");
  $("#timeFin").val("");
  $("#recurrence").val("Ponctuelle");
  $('#overlay').css('display', 'none');
  $("#Valider").off("click");
}

function afficherDispo(dispo) {
  console.log(dispo.nounou.email);
  if(dispo.nounou.email == $('#email').text()){
    let newDispo = {"id":dispo.id, "title": "Libre", "start":moment(dispo.datedebut.date).format(), "end":moment(dispo.datefin.date).format()};
    if(dispo.accepte){
      newDispo = {"color":"rgb(240, 127, 44)","id":"0", "title": "Garde chez "+dispo.parent.prenom+" "+dispo.parent.nom, "start":moment(dispo.datedebut.date).format(), "end":moment(dispo.datefin.date).format()};
    }
    datas.push(newDispo);
    if(dispo.parent && !$("#bloque").val() && !dispo.accepte){
      notifs.push(dispo);
    }
  }
}

function creerNotification(dispo){
  $('.carteNotif').empty();
  var notif = "<div id='"+dispo.id+"' class='carte'><h1>Nouvelle Notification !</h1><div><h4><b>Créneau : du "+moment(dispo.datedebut.date).format("d/MM/YYYY à HH:mm")+" au "+moment(dispo.datefin.date).format("d/MM/YYYY à HH:mm")+"</b></h4><h4><b>Parent :</b> "+dispo.parent.prenom+" "+dispo.parent.nom+"</h4><h4><b>Enfants :</b> ";
  dispo.parent.enfants.forEach(function(enfant){
    notif += "<br>Prénom : "+enfant.prenom+"<br/>Age : "+moment(enfant.dateNaissance.date).fromNow()+"<br/>Restriction : "+enfant.restrictionAlimentaire+"<br>";
  });
  notif += "</ul></h4></div>";
  $('.carteNotif').append(notif);
}

function nextNotification(){
  notifActuelle  = (notifActuelle+1)%notifs.length;
  creerNotification(notifs[notifActuelle]);
  console.log(notifActuelle);
}

function prevNotification(){
  notifActuelle  = (notifActuelle-1)%notifs.length;
  creerNotification(notifs[notifActuelle]);
  console.log(notifActuelle);
}

function accepterReservation(){
	let carte = $(".carte").length;
	if(carte != 0){
    $("#accepter").on("click", function(){
      id = $(".carte").attr('id');
      $.ajax({
        url : "/traitement/disponibilite/"+id+"/valider",
        success: function(){
          actualiserDispo();
        }
      });
    });
	}else{
    $("#accepter").attr('disabled', 'disabled');
  }
}

function refuserReservation(){
  let carte = $(".carte").length;
	if(carte != 0){
    $("#refuser").on("click", function(){
      id = $(".carte").attr('id');
      $.ajax({
        url : "/traitement/disponibilite/"+id+"/refuser",
        success: function(){
          actualiserDispo();
        }
      });
    });
	}else{
    $("#refuser").attr('disabled', 'disabled');
  }
}
