var enfantSelectionne = null;
$(function(){
  $('#btnAjouter').on("click", afficherAjouterEnfant);
  $('.btnNoter').on("click", function(){
    $('#idRes').val($(this).attr('id'));
    afficherNoterDispo();
  });
	$('#validerAjout').on("click", ajouterEnfant);
  $('#validerNotation').on("click", ajouterNotation);
  $('#valideRecherche').on('click', function(){
    document.location.href="/traitement/recherche/nounou?ville="+$('#ville').val()+"&dateDebut="+$('#dateDebut').val()+"&dateFin="+$('#dateFin').val()+"&langue="+$('#langue').val();
  })
  $(document).keypress(function(e){
    if (e.which == 0){
      $('#overlayEnfant').css("display", "none");
      $('#overlayNotation').css("display", "none");
    }
  });
  actualiserEnfants();
});

function ajouterEnfant() {
	let prenom = $('#nomEnf').val();
	let dateNaissance = $('#naissEnf').val();
	let restrictions = $('#restEnf').val();
	$.ajax({
		url: '/traitement/parent/enfant/ajouter',
		method: "POST",
		data: "prenom="+prenom+"&naissance="+dateNaissance+"&restriction="+restrictions,
		success: (res) => {
			$('#overlayEnfant').css("display", "none");
      $('#restEnf').val("");
      $('#naissEnf').val("");
      $('#nomEnf').val("");
		},
		error: (err) => {
			console.log(err)
		}
	})
	actualiserEnfants();
}

function afficherAjouterEnfant(){
  $('#overlayEnfant').css("display", "block");
}
function afficherNoterDispo(){
  $('#overlayNotation').css("display", "block");
}
function actualiserEnfants(){
  let id = $("#id").val();
	$(".listeEnfant").empty()
  console.log(id);
  $.ajax({
    url:"/donnees/parents/enfants/"+id,
    success: function(result){
      let pos = 0;
      result.forEach(function(enfant){
        let date = enfant.dateNaissance.date.split(" ");
        afficheEnfant(pos, enfant.id, enfant.prenom, date[0], enfant.restrictionAlimentaire);
        pos++;
      })
    }
  })
}

function afficheEnfant(position, id, prenom, dateNaissance, restrictions){
	var enfant = "<div class='enfant' id='"+position+"'>"
	enfant+="<button id='"+id+"'class='boutonSupprimer btn btn-danger'>Supprimer</button>"
	enfant+="<input type='hidden' id='id"+position+"' value='"+id+"'/>"
	enfant+="<div class='champ'>Nom : "+prenom+"<div/>"
	enfant+="<div class='champ'>Date de Naissance : "+dateNaissance+"<div/>"
	enfant+="<div class='champ'>Restrictions Alimentaires : "+restrictions+"<div/>"
	enfant+="<div/>";
  $(".listeEnfant").append(enfant);
  $('#'+position+'').on("click", function(){
    if(enfantSelectionne != null){
      $('#'+enfantSelectionne).css("background-color", "#FFF");
      $('#'+enfantSelectionne).css("color", "#000");
    }
    enfantSelectionne = position;
    $('#'+position).css("background-color", "rgba(72,153,228, 1)");
    $('#'+position).css("color", "#FFF");
  });
  $('.boutonSupprimer').off("click");
  $('.boutonSupprimer').on("click", function(){
    supprimer($(this).attr('id'));
  });
}

function supprimer(id){
  $.ajax({
    url:"/traitement/enfant/"+id+"/supprimer",
    success: function(result){
      actualiserEnfants();
    }
  })
}

function ajouterNotation(){
  if($("#idRes").val() != ""){
    let id = $("#idRes").val();
    $.ajax({
      url: "/traitement/disponibilite/"+id+"/noter",
      type: "GET",
      data: "note="+$('#Note').val(),
      success: function(){
        document.location.href = "/";
      }
    });

  }
}
