<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Langue
 *
 * @ORM\Table(name="langue", uniqueConstraints={@ORM\UniqueConstraint(name="intitule", columns={"intitule"})})
 * @ORM\Entity
 */
class Langue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="intitule", type="string", length=255, nullable=false)
     */
    private $intitule;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Nounou", mappedBy="langues")
	 * @ORM\JoinColumn(name="Nounou", referencedColumnName="utilisateur")
     */
    private $nounous;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->nounou = new \Doctrine\Common\Collections\ArrayCollection();
        $this->nounous = new ArrayCollection();
    }

 public function addNounou(Nounou $nounou) :self {
	 if(!$this->nounou->contains($nounou)) {
		 $this->nounou[] = $nounou;
		 $nounou->addLangue($this);
	 }
	 return $this;
 }

    /**
     * Get the value of Intitule
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set the value of Intitule
     *
     * @param string intitule
     *
     * @return self
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get the value of Nounou
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNounou()
    {
        return $this->nounou;
    }

    /**
     * Set the value of Nounou
     *
     * @param \Doctrine\Common\Collections\Collection nounou
     *
     * @return self
     */
    public function setNounou(\Doctrine\Common\Collections\Collection $nounou)
    {
        $this->nounou = $nounou;

        return $this;
    }

    /**
     * @return Collection|Nounou[]
     */
    public function getNounous(): Collection
    {
        return $this->nounous;
    }

    public function addNounous(Nounou $nounous): self
    {
        if (!$this->nounous->contains($nounous)) {
            $this->nounous[] = $nounous;
            $nounous->addLangue($this);
        }

        return $this;
    }

    public function removeNounous(Nounou $nounous): self
    {
        if ($this->nounous->contains($nounous)) {
            $this->nounous->removeElement($nounous);
            $nounous->removeLangue($this);
        }

        return $this;
    }

}
