<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Interfaces\ExportableInterface;
use App\Entity\Parents;

/**
 * Enfant
 *
 * @ORM\Table(name="enfant", indexes={@ORM\Index(name="idx_enfant__parents", columns={"parents"})})
 * @ORM\Entity
 */
class Enfant implements ExportableInterface
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="prenom", type="string", length=255, nullable=false)
	 */
	private $prenom;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="datenaissance", type="date", nullable=false)
	 */
	private $datenaissance;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $restrictionsAlimentaires;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Parents", inversedBy="enfants")
	 * @ORM\JoinColumn(name="parents", referencedColumnName="utilisateur", nullable=false)
	 */
	private $parents;



	/**
	 * Get the value of Id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Get the value of Prenom
	 *
	 * @return string
	 */
	public function getPrenom()
	{
		return $this->prenom;
	}

	/**
	 * Set the value of Prenom
	 *
	 * @param string prenom
	 *
	 * @return self
	 */
	public function setPrenom($prenom)
	{
		$this->prenom = $prenom;

		return $this;
	}

	/**
	 * Get the value of Datenaissance
	 *
	 * @return \DateTime
	 */
	public function getDatenaissance()
	{
		return $this->datenaissance;
	}

	/**
	 * Set the value of Datenaissance
	 *
	 * @param \DateTime datenaissance
	 *
	 * @return self
	 */
	public function setDatenaissance(\DateTime $datenaissance)
	{
		$this->datenaissance = $datenaissance;

		return $this;
	}

	/**
	 * Get the value of Parents
	 *
	 * @return \Parents
	 */
	public function getParents()
	{
		return $this->parents;
	}

	/**
	 * Set the value of Parents
	 *
	 * @param \Parents parents
	 *
	 * @return self
	 */
	public function setParents(Parents $parents)
	{
		$this->parents = $parents;

		return $this;
	}


	public function getTableauDeDonnees() : array{
		return array(
			"prenom" => $this->getPrenom(),
			"date_naissance" => $this->getDatenaissance(),
			"restriction_alimentaire" => $this->getRestrictionsAlimentaires()
		);
	}

	public function getRestrictionsAlimentaires(): ?string
	{
		return $this->restrictionsAlimentaires;
	}

	public function setRestrictionsAlimentaires(?string $restrictionsAlimentaires): self
	{
		$this->restrictionsAlimentaires = $restrictionsAlimentaires;

		return $this;


	}

	public function getParent(): ?Parents
	{
		return $this->parent;
	}

	public function setParent(?Parents $parent): self
	{
		$this->parent = $parent;
		return $this;
	}

}
