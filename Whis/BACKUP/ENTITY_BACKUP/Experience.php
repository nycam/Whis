<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Experience
 *
 * @ORM\Table(name="experience", indexes={@ORM\Index(name="idx_experience__nounou", columns={"nounou"})})
 * @ORM\Entity
 */
class Experience
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="experience", type="string", length=140, nullable=false)
     */
    private $experience;

    /**
     * @var \Nounou
     *
     * @ORM\ManyToOne(targetEntity="Nounou")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nounou", referencedColumnName="utilisateur")
     * })
     */
    private $nounou;



    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Experience
     *
     * @return string
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set the value of Experience
     *
     * @param string experience
     *
     * @return self
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get the value of Nounou
     *
     * @return \Nounou
     */
    public function getNounou()
    {
        return $this->nounou;
    }

    /**
     * Set the value of Nounou
     *
     * @param \Nounou nounou
     *
     * @return self
     */
    public function setNounou(\Nounou $nounou)
    {
        $this->nounou = $nounou;

        return $this;
    }

}
