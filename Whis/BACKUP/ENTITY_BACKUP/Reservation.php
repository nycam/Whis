<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation", indexes={@ORM\Index(name="idx_reservation__disponibilite", columns={"disponibilite"}), @ORM\Index(name="idx_reservation__enfant", columns={"enfant"}), @ORM\Index(name="idx_reservation__langue", columns={"langue"})})
 * @ORM\Entity
 */
class Reservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $note = 'NULL';

    /**
     * @var \Disponibilite
     *
     * @ORM\ManyToOne(targetEntity="Disponibilite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="disponibilite", referencedColumnName="id")
     * })
     */
    private $disponibilite;

    /**
     * @var \Enfant
     *
     * @ORM\ManyToOne(targetEntity="Enfant")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="enfant", referencedColumnName="id")
     * })
     */
    private $enfant;

    /**
     * @var \Langue
     *
     * @ORM\ManyToOne(targetEntity="Langue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="langue", referencedColumnName="id")
     * })
     */
    private $langue;



    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Note
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set the value of Note
     *
     * @param string|null note
     *
     * @return self
     */
    public function setNote(string $note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get the value of Disponibilite
     *
     * @return \Disponibilite
     */
    public function getDisponibilite()
    {
        return $this->disponibilite;
    }

    /**
     * Set the value of Disponibilite
     *
     * @param \Disponibilite disponibilite
     *
     * @return self
     */
    public function setDisponibilite(\Disponibilite $disponibilite)
    {
        $this->disponibilite = $disponibilite;

        return $this;
    }

    /**
     * Get the value of Enfant
     *
     * @return \Enfant
     */
    public function getEnfant()
    {
        return $this->enfant;
    }

    /**
     * Set the value of Enfant
     *
     * @param \Enfant enfant
     *
     * @return self
     */
    public function setEnfant(\Enfant $enfant)
    {
        $this->enfant = $enfant;

        return $this;
    }

    /**
     * Get the value of Langue
     *
     * @return \Langue
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * Set the value of Langue
     *
     * @param \Langue langue
     *
     * @return self
     */
    public function setLangue(\Langue $langue)
    {
        $this->langue = $langue;

        return $this;
    }

}
