<?php

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use App\Interfaces\ExportableInterface;

/**
 * Nounou
 *
 * @ORM\Table(name="nounou", uniqueConstraints={@ORM\UniqueConstraint(name="portable", columns={"portable"})})
 * @ORM\Entity(repositoryClass="App\Repository\NounouRepo")
 */
class Nounou implements ExportableInterface
{
	/**
	 * @var string
	 * @Assert\NotBlank()
	 * @Assert\Length(min=10, max=10)
	 * @ORM\Column(name="portable", type="string", length=255, nullable=false)
	 */
	private $portable;

	/**
	 * @var string
	 * @ORM\Column(name="photo", type="string", length=255, nullable=false)
	 */
	private $photo;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="datenaissance", type="date", nullable=false)
	 */
	private $datenaissance;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="presentation", type="string", length=255, nullable=false)
	 */
	private $presentation;

	/**
	 * @var \Utilisateur
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="Utilisateur", cascade={"persist"})
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="utilisateur", referencedColumnName="id")
	 * })
	 */
	private $utilisateur;

	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\Langue", inversedBy="nounous")
	 * @ORM\JoinColumn(name="Langue", referencedColumnName="id")
	 */
	private $langues;

	public function __construct()
	{
		$this->langues = new ArrayCollection();
	}

	/**
	 * @return self
	 */
	public function setPhoto($photo)
	{
		$this->photo = $photo;

		return $this;
	}

	/**
	 * Get the value of Datenaissance
	 *
	 * @return \DateTime
	 */
	public function getDatenaissance()
	{
		return $this->datenaissance;
	}

	/**
	 * Set the value of Datenaissance
	 *
	 * @param \DateTime datenaissance
	 *
	 * @return self
	 */
	public function setDatenaissance(\DateTime $datenaissance) :self
	{
		$this->datenaissance = $datenaissance;

		return $this;
	}

	/**
	 * Get the value of Presentation
	 *
	 * @return string
	 */
	public function getPresentation()
	{
		return $this->presentation;
	}

	/**
	 * Set the value of Presentation
	 *
	 * @param string presentation
	 *
	 * @return self
	 */
	public function setPresentation($presentation) :self 
	{
		$this->presentation = $presentation;

		return $this;
	}

	/**
	 * Get the value of Langue
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getLangue()
	{
		$langues = array();
		foreach($this->langue as $langue) {
			$langues[] = $langue->getIntitule();
		}

		return $langues;
	}

	/**
	 * Set the value of Langue
	 *
	 * @return self
	 */
	public function setLangue(/*array*/ $tmpLang) :self 
	{

		foreach($tmpLang as $key => $langue) {
			$this->langue[$key] = $langue;
		}

		return $this;
	}


	/**
	 * Get the value of Utilisateur
	 *
	 * @return \Utilisateur
	 */
	public function getUtilisateur()
	{
		return $this->utilisateur;
	}

	public function setUtilisateur(Utilisateur $utilisateur) :self {
		$this->utilisateur = $utilisateur;
		return $this;
	}

	public function getTableauDeDonnees() : array{
		$jsonUtilisateur = $this->getUtilisateur()->getTableauDeDonnees();
		$jsonNounou = array(
			"nom" => $this->getUtilisateur()->getNom(),
			"prenom" => $this->getUtilisateur()->getPrenom(),
			"portable" => $this->getPortable(),
			"photo" => $this->getPhoto(),
			"date_naissance" => $this->getDatenaissance(),
			"presentation" => $this->getPresentation(),
			"langue" => $this->getLangue()
		);

		return array_merge($jsonUtilisateur, $jsonNounou);
	}

	public function getValide(): ?bool
	{
		return $this->valide;
	}

	public function setValide(bool $valide): self
	{
		$this->valide = $valide;
		return $this;
	}

	/**
	 * @return Collection|Disponibilite[]
	 */
	public function getDisponibilites(): Collection
	{
		return $this->disponibilites;
	}

	public function addLangue(Langue $langue) :self {
		if(!$this->langue->contains($langue)) {
			$this->langue[] = $langue;
			$langue->addNounou($this);
		}
		return $this;
	}

	public function addDisponibilite(Disponibilite $disponibilite): self
	{
		if (!$this->disponibilites->contains($disponibilite)) {
			$this->disponibilites[] = $disponibilite;
			$disponibilite->setProprietaire($this);
		}
		return $this;
	}

	public function removeDisponibilite(Disponibilite $disponibilite): self
	{
		if ($this->disponibilites->contains($disponibilite)) {
			$this->disponibilites->removeElement($disponibilite);
			// set the owning side to null (unless already changed)
			if ($disponibilite->getProprietaire() === $this) {
				$disponibilite->setProprietaire(null);
			}
		}
		return $this;
	}

	/**
	 * @return Collection|Langue[]
	 */
	public function getLangues(): Collection
	{
		return $this->langues;
	}

	public function removeLangue(Langue $langue): self
	{
		if ($this->langues->contains($langue)) {
			$this->langues->removeElement($langue);
		}
		return $this;
	}

}
