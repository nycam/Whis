<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Role;
use Doctrine\Common\Collections\ArrayCollection;
use App\Interfaces\ExportableInterface;


/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur", uniqueConstraints={@ORM\UniqueConstraint(name="email", columns={"email"})})
 * @ORM\Entity
 */
class Utilisateur implements UserInterface, ExportableInterface, \Serializable
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="nom", type="string", length=255, nullable=false)
	 */
	private $nom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="prenom", type="string", length=255, nullable=false)
	 */
	private $prenom;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=255, nullable=false)
	 */
	private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ville", type="string", length=255, nullable=false)
	 */
	private $ville;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="mdphash", type="string", length=255, nullable=false)
	 */
	private $mdphash;

	/**
	 * @var \Doctrine\Common\Collections\Collection
	 *
	 * @ORM\ManyToMany(targetEntity="Role", cascade={"persist"},inversedBy="utilisateur")
	 * @ORM\JoinTable(name="role_utilisateurs",
	 *   joinColumns={
	 *     @ORM\JoinColumn(name="utilisateur", referencedColumnName="id")
	 *   },
	 *   inverseJoinColumns={
	 *     @ORM\JoinColumn(name="role", referencedColumnName="id")
	 *   }
	 * )
	 */
	private $role;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->role = new \Doctrine\Common\Collections\ArrayCollection();
	}

	public function setNounou($nounou) :self
	{
		$this->nounou = $nounou;
		return $this;
	}

	public function getNounou()
	{
		return $this->nounou;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setVille($ville) : self
	{
		$this->ville = $ville;
		return $this;
	}

	public function getVille()
	{
		return $this->ville;
	}

	public function setMdphash(string $pass) :self {
		$this->mdphash = $pass;
		return $this;	
	}

	#getter
	public function getMdphash() : ?string {
		return $this->mdphash;
	}


	public function getRoles() {
	}

	}


	/**
	 * Équivalent à setMdphash, implémentation de UserInterface
	 */
	public function setPassword($hashPass): self{
		return $this->setMdphash($hashPass);
	}

	public function setNom(string $_nom) : self{
		$this->nom = $_nom;
		return $this;
	}

	public function getNom() : ?string {
		return $this->nom;
	}

	public function getPrenom() : ?string {
		return $this->prenom;
	}

	public function setPrenom(String $_prenom) :self {
		$this->prenom = $_prenom;
		return $this;
	}

	public function setEmail(String $_email) :self {
		$this->email = $_email;
		return $this;
	}

	public function getEmail() :?string {
		return $this->email;
	}

	public function getRole() {
		return $this->role;
	}

	public function addRole(Role $role) {
		$this->role[] = $role;
		return $this;
	}







	public function getParent(): ?Parents
	{
		return $this->parent;
	}

	public function setParent(?Parents $parent): self
	{
		$this->parent = $parent;
		return $this;
	}

}
