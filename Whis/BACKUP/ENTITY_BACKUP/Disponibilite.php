<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Nounou;

/**
 * Disponibilite
 *
 * @ORM\Table(name="disponibilite", indexes={@ORM\Index(name="idx_disponibilite__nounou", columns={"nounou"})})
 * @ORM\Entity
 */
class Disponibilite
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datedebut", type="datetime")
     */
    private $datedebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefin", type="datetime", nullable=false)
     */
    private $datefin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Nounou", inversedBy="disponibilites")
     * @ORM\JoinColumn(name="Nounou", nullable=false, referencedColumnName="utilisateur")
     */
    private $proprietaire;



    /**
     * Get the value of Id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Datedebut
     *
     * @return \DateTime|null
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    /**
     * Set the value of Datedebut
     *
     * @param \DateTime|null datedebut
     *
     * @return self
     */
    public function setDatedebut(\DateTime $datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get the value of Datefin
     *
     * @return \DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set the value of Datefin
     *
     * @param \DateTime datefin
     *
     * @return self
     */
    public function setDatefin(\DateTime $datefin)
    {
        $this->datefin = $datefin;

        return $this;
    }


    public function getProprietaire(): ?Nounou
    {
        return $this->proprietaire;
    }

    public function setProprietaire(?Nounou $proprietaire): self
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }

}
