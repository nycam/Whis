<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Utilisateur;
use App\Interfaces\ExportableInterface;

/**
 * Parents
 *
 * @ORM\Table(name="parents")
 * @ORM\Entity
 */
class Parents implements ExportableInterface
{
	/**
	 * @var string
	 *
	 * @ORM\Column(name="information", type="string", length=255, nullable=false)
	 */
	private $information;

	/**
	 * @var \Utilisateur
	 *
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="NONE")
	 * @ORM\OneToOne(targetEntity="Utilisateur", cascade={"persist"})
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="utilisateur", referencedColumnName="id")
	 * })
	 */
	private $utilisateur;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Enfant", mappedBy="parent", orphanRemoval=true)
	 */
	private $enfants;

	public function __construct()
	{
		$this->enfants = new ArrayCollection();
	}



	/**
	 * Get the value of Information
	 *
	 * @return string
	 */
	public function getInformation()
	{
		return $this->information;
	}

	/**
	 * Set the value of Information
	 *
	 * @param string information
	 *
	 * @return self
	 */
	public function setInformation($information)
	{
		$this->information = $information;

		return $this;
	}

	/**
	 * Get the value of Utilisateur
	 *
	 * @return \Utilisateur
	 */
	public function getUtilisateur()
	{
		return $this->utilisateur;
	}

	public function setUtilisateur(Utilisateur $utilisateur) :self {
		$this->utilisateur = $utilisateur;
		return $this;
	}

	public function getTableauDeDonnees() : array {
		$tabUtilisateur = $this->utilisateur->getTableauDeDonnees();
		$jsonParents = array (
			"info" => $this->information
		);

		return array_merge($tabUtilisateur, $jsonParents);
	}

	/**
	 * @return Collection|Enfant[]
	 */
	public function getEnfants(): Collection
	{
		return $this->enfants;
	}

	public function addEnfant(Enfant $enfant): self
	{
		if (!$this->enfants->contains($enfant)) {
			$this->enfants[] = $enfant;
			$enfant->setParent($this);
		}
		return $this;
	}

	public function removeEnfant(Enfant $enfant): self
	{
		if ($this->enfants->contains($enfant)) {
			$this->enfants->removeElement($enfant);
			// set the owning side to null (unless already changed)
			if ($enfant->getParent() === $this) {
				$enfant->setParent(null);
			}
		}
		return $this;
	}

}
