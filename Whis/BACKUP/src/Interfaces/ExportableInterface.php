<?php
namespace App\Interfaces;

interface ExportableInterface {
  public function getTableauDeDonnees() : array;
}
