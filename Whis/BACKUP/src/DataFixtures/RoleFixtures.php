<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Role;

class RoleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
		$ROLE = ["ROLE_ADMIN", "ROLE_NOUNOU", "ROLE_PARENT"];

		foreach($ROLE as $value) {
			$role = new Role();
			$role->setIntitule($value);
			$manager->persist($role);
			$this->setReference($value, $role);
		}

        $manager->flush();
    }
}
