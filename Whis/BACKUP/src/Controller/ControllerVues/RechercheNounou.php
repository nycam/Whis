<?php

namespace App\Controller\ControllerVues;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RechercheNounou extends Controller
{
    /**
     * @Route("vue/nounou/recherche", name="recherche_nounou_vue")
     */
    public function index()
    {
        return $this->render('recherche_nounou/index.html.twig');
    }
}
