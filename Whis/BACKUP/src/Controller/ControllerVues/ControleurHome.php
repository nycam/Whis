<?php

namespace App\Controller\ControllerVues;

use Symfony\Component\Debug\Debug;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Nounou;
use App\Entity\Parents;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ControleurHome extends Controller{

	/**
	 * @Route("/vue/accueil", name="accueil")
	 */
	public function AccueilUtilisateurConnecte(){
		//Si l'utilisateur est connecté
		$this->denyAccessUnlessGranted("IS_AUTHENTICATED_FULLY");
		$user = $this->getUser();
		$roles = $user->getRoles();
		$image = null;
		$nounou = $this->getDoctrine()
			->getRepository(Nounou::class)
			->findOneBy(['utilisateur'=>$user->getId()]);
		$parent = $this->getDoctrine()
			->getRepository(Parents::class)
			->findOneBy(["utilisateur"=>$user->getId()]);

		$url = "home/utilisateurNonConnecte.html.twig";
		if(in_array("ROLE_NOUNOU", $roles) && $nounou){
			$image = $nounou->getPhoto();
			$url = "home/dashboardNounou.html.twig";
		}
		if($parent){
			$url = "home/dashboardParent.html.twig";
		}
		return $this->render($url, array('utilisateur' => $user, 'roles' => $roles, 'image' => $image));
	}

	/**
	 *@Route("/", name="root")
	 */
	public function AccueilUtilisateurNonConnecte( AuthorizationCheckerInterface $auth) {
		if($auth->isGranted('ROLE_ADMIN') || $auth->isGranted('ROLE_NOUNOU') || $auth->isGranted('ROLE_PARENT')){
			return $this->redirectToRoute('Accueil');
		}
		return $this->render('home/utilisateurNonConnecte.html.twig', array('utilisateur' => null));
	}
}
