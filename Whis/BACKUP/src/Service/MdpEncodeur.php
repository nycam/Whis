<?php

namespace App\Service;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Entity\Nounou;

class MdpEncodeur {

	private $encoder;

	public function __construct(UserPasswordEncoderInterface $encoder) {
		$this->encoder = $encoder;
	}
	public function encoder(UserInterface $utilisateur){
		$mdp = $utilisateur->getPassword();
		$mdp = $this->encoder->encodePassword($utilisateur, $mdp);
		return $mdp;
	}
}
?>
