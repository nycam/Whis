<?php

namespace App\Form;

use App\Entity\Nounou;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Langue;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class NounouFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		//Recuperation des langues
		$manager = $options['entity_manager'];
		$tuples = $manager->getRepository(Langue::class)->findAll();
		$langues = new ArrayCollection();
		foreach($tuples as $langue) {
			$langues[$langue->getIntitule()] = $langue;
		}
		$builder
			->add('utilisateur', UtilisateurFormType::class)
			->add('portable', null, ["label" => "Portable"])
			->add('photo', FileType::Class, [
				"label" => "Photo de profil",
				"data_class" => null
			])
			->add('datenaissance', DateType::Class, ["label"=> "Date de naissance", "widget"=>"single_text"])
			->add('presentation', TextareaType::class)
			->add('langues', ChoiceType::class, [
				"label"=> "Langues que je maitrise :",
				"choices" => $langues,
				"multiple" => "multiple"
			]
		)
		->add('envoyer', SubmitType::class)
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Nounou::class,
		]);
		$resolver->setRequired('entity_manager');
	}
}
