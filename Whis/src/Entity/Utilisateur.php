<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Interfaces\ExportableInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UtilisateurRepository")
 */
class Utilisateur implements UserInterface, ExportableInterface, \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mdp;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role")
     */
    private $roles;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Parents", mappedBy="utilisateur", cascade={"persist", "remove"})
     */
    private $parent;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Nounou", mappedBy="utilisateur", cascade={"persist", "remove"})
     */
    private $nounou;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $ville;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMdp(): ?string
    {
        return $this->mdp;
    }

    public function setMdp(string $mdp): self
    {
        $this->mdp = $mdp;

        return $this;
    }


    public function removeRole(Role $role): self
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }

    public function getParent(): ?Parents
    {
        return $this->parent;
    }

    public function setParent(Parents $parent): self
    {
        $this->parent = $parent;

        // set the owning side of the relation if necessary
        if ($this !== $parent->getUtilisateur()) {
            $parent->setUtilisateur($this);
        }

        return $this;
    }

    public function getNounou(): ?Nounou
    {
        return $this->nounou;
    }

    public function setNounou(Nounou $nounou): self
    {
        $this->nounou = $nounou;

        // set the owning side of the relation if necessary
        if ($this !== $nounou->getUtilisateur()) {
            $nounou->setUtilisateur($this);
        }

        return $this;
    }

	/*User Interface implementation*/

    /**
     * @return Collection|Role[]
     */
    public function getRoles(): array
    {
		$nomsRole = [];
		foreach ($this->roles as $value) {
			$nomsRole[] = $value->getIntitule();
		}
		return $nomsRole;
    }

    public function addRole(Role $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

	public function getSalt() {
		return null;
	}

	public function getUsername() {
		return $this->email;
	}

	//unused
	public function eraseCredentials() {	}

	public function getPassword() :?string {
		return $this->mdp;
	}

	public function setPassword(string $password) : self{
		$this->mdp = $password;
		return $this;
	}

	/*Serialize implementation*/
		
	public function serialize()
	{
		return serialize(array(
			$this->id,
			$this->mdp,
			$this->email,
		));
	}

	public function unserialize($serialized)
	{
		list (
			$this->id,
			$this->mdp,
			$this->email,
		) = unserialize($serialized, ['allowed_classes' => false]);
	}


	/*ExportableInterface implementation*/
	public function getTableauDeDonnees() : array{
		return array(
			"nom" => $this->nom,
			"prenom" => $this->prenom,
			"email" => $this->email,
			"ville" => $this->ville
		);
	}

 public function getVille(): ?string
 {
     return $this->ville;
 }

 public function setVille(string $ville): self
 {
     $this->ville = $ville;
     return $this;
 }

}
