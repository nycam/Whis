<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Interfaces\ExportableInterface;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParentsRepository")
 */
class Parents implements ExportableInterface
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $information;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Utilisateur", inversedBy="parent", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $utilisateur;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Enfant", mappedBy="parents", orphanRemoval=true)
	 */
	private $enfants;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Disponibilite", mappedBy="parent")
	 */
	private $creneauxReserves;


	public function __construct()
	{
		$this->enfants = new ArrayCollection();
		$this->creneauxReserves = new ArrayCollection();
	}

	public function getId()
	{
		return $this->id;
	}

	public function getInformation(): ?string
	{
		return $this->information;
	}

	public function setInformation(?string $information): self
	{
		$this->information = $information;

		return $this;
	}

	public function getUtilisateur(): ?Utilisateur
	{
		return $this->utilisateur;
	}

	public function setUtilisateur(Utilisateur $utilisateur): self
	{
		$this->utilisateur = $utilisateur;

		return $this;
	}

	public function getEnfants() : ?PersistentCollection
	{
		return $this->enfants;
	}

	public function setEnfants(?Enfant $enfants): self
	{
		$this->enfants = $enfants;

		return $this;
	}

	public function addEnfant(Enfant $enfant): self
	{
		if (!$this->enfants->contains($enfant)) {
			$this->enfants[] = $enfant;
			$enfant->setParents($this);
		}

		return $this;
	}

	public function removeEnfant(Enfant $enfant): self
	{
		if ($this->enfants->contains($enfant)) {
			$this->enfants->removeElement($enfant);
			// set the owning side to null (unless already changed)
			if ($enfant->getParents() === $this) {
				$enfant->setParents(null);
			}
		}

		return $this;
	}

	public function getTableauDeDonnees() : array {
		$tabUtilisateur = $this->utilisateur->getTableauDeDonnees();
		$jsonParents = array (
			"info" => $this->information
		);
		$enfants = [];
		foreach ($this->enfants as $enfant) {
			$enfants[] = $enfant->getDatas();
		}
		$jsonParents["enfants"] = $enfants;

		return array_merge($tabUtilisateur, $jsonParents);
	}
	/**
	 * @return Collection|Disponibilite[]
	 */
	public function getCreneauxReserves(): Collection
	{
		return $this->creneauxReserves;
	}

	public function addCreneauxReserves(Disponibilite $creneauxReserf): self
	{
		if (!$this->creneauxReserves->contains($creneauxReserf)) {
			$this->creneauxReserves[] = $creneauxReserf;
			$creneauxReserf->setParent($this);
		}
		return $this;
	}

	public function removeCreneauxReserves(Disponibilite $creneauxReserf): self
	{
		if ($this->creneauxReserves->contains($creneauxReserf)) {
			$this->creneauxReserves->removeElement($creneauxReserf);
			// set the owning side to null (unless already changed)
			if ($creneauxReserf->getParent() === $this) {
				$creneauxReserf->setParent(null);
			}
		}
		return $this;
	}
}
