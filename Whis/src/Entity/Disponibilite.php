<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Interfaces\ExportableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DisponibiliteRepository")
 */
class Disponibilite implements ExportableInterface
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $datedebut;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $datefin;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Nounou", inversedBy="disponibilite")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $nounou;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Parents", inversedBy="creneauxReserves")
	 */
	private $parent;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $accepte;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $note;


	public function __construct()
	{
		$this->accepte = false;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getDatedebut(): ?\DateTimeInterface
	{
		return $this->datedebut;
	}

	public function setDatedebut(\DateTimeInterface $datedebut): self
	{
		$this->datedebut = $datedebut;

		return $this;
	}

	public function getDatefin(): ?\DateTimeInterface
	{
		return $this->datefin;
	}

	public function setDatefin(\DateTimeInterface $datefin): self
	{
		$this->datefin = $datefin;

		return $this;
	}

	public function getNounou(): ?Nounou
	{
		return $this->nounou;
	}

	public function setNounou(?Nounou $nounou): self
	{
		$this->nounou = $nounou;

		return $this;
	}

	public function getParent(): ?Parents
	{
		return $this->parent;
	}

	public function setParent(?Parents $parent): self
	{
		$this->parent = $parent;

		return $this;
	}

	public function getAccepte(): ?bool
	{
		return $this->accepte;
	}

	public function setAccepte(bool $accepte): self
	{
		$this->accepte = $accepte;

		return $this;
	}


	public function getTableauDeDonnees() :array {
		return [
			"id" => $this->id,
			"note" => $this->note,
			"datedebut" => $this->datedebut,
			"datefin" => $this->datefin,
			"parent" => $this->parent==null?null:$this->parent->getTableauDeDonnees(),
			"nounou" => $this->nounou->getTableauDeDonnees(),
			"accepte" => $this->accepte
		];
	}

	public function getNote(): ?int
	{
		return $this->note;
	}

	public function setNote(?int $note): self
	{
		$this->note = $note;
		return $this;
	}
}
