<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class Langue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $intitule;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Nounou", mappedBy="langues")
     */
    private $nounous;

    public function __construct()
    {
        $this->nounous = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIntitule(): ?string
    {
        return $this->intitule;
    }

    public function setIntitule(string $intitule): self
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * @return Collection|Nounou[]
     */
    public function getNounous(): Collection
    {
        return $this->nounous;
    }

    public function addNounous(Nounou $nounous): self
    {
        if (!$this->nounous->contains($nounous)) {
            $this->nounous[] = $nounous;
            $nounous->addLangue($this);
        }

        return $this;
    }

    public function removeNounous(Nounou $nounous): self
    {
        if ($this->nounous->contains($nounous)) {
            $this->nounous->removeElement($nounous);
            $nounous->removeLangue($this);
        }

        return $this;
    }
}
