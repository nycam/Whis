<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EnfantRepository")
 */
class Enfant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $prenom;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datenaissance;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $restrictions_alimentaires;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Parents", inversedBy="enfants")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parents;


    public function __construct()
    {
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getDatenaissance(): ?\DateTimeInterface
    {
        return $this->datenaissance;
    }

    public function setDatenaissance(\DateTimeInterface $datenaissance): self
    {
        $this->datenaissance = $datenaissance;

        return $this;
    }

    public function getRestrictionsAlimentaires(): ?string
    {
        return $this->restrictions_alimentaires;
    }

    public function setRestrictionsAlimentaires(?string $restrictions_alimentaires): self
    {
        $this->restrictions_alimentaires = $restrictions_alimentaires;

        return $this;
    }

    public function getParents(): ?Parents
    {
        return $this->parents;
    }

    public function setParents(?Parents $parents): self
    {
        $this->parents = $parents;

        return $this;
    }

    public function getDatas()
    {
      $data = [];
      $data["id"] = $this->id;
      $data["prenom"] = $this->prenom;
      $data["dateNaissance"] = $this->datenaissance;
      $data["restrictionAlimentaire"] = $this->restrictions_alimentaires;

      return $data;
    }



}
