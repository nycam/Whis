<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Interfaces\ExportableInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NounouRepository")
 */
class Nounou implements ExportableInterface
{
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\OneToOne(targetEntity="App\Entity\Utilisateur", inversedBy="nounou", cascade={"persist", "remove"})
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $utilisateur;

	/**
	 * @ORM\Column(type="string", length=10)
	 */
	private $portable;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $photo;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $presentation;

	/**
	 * @ORM\ManyToMany(targetEntity="App\Entity\Langue", inversedBy="nounous")
	 */
	private $langues;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Disponibilite", mappedBy="nounou", orphanRemoval=true)
	 */
	private $disponibilite;
	/**
	 * @ORM\Column(type="boolean")
	 */
	private $valide;
	/**
	 * @ORM\Column(type="date")
	 */
	private $datenaissance;

	public function __construct()
	{
		$this->langues = new ArrayCollection();
		$this->disponibilite = new ArrayCollection();
		$this->valide = false;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getUtilisateur(): ?Utilisateur
	{
		return $this->utilisateur;
	}

	public function setUtilisateur(Utilisateur $utilisateur): self
	{
		$this->utilisateur = $utilisateur;

		return $this;
	}

	public function getPortable(): ?string
	{
		return $this->portable;
	}

	public function setPortable(string $portable): self
	{
		$this->portable = $portable;

		return $this;
	}

	public function getPhoto()
	{
		return $this->photo;
	}

	public function setPhoto($photo): self
	{
		$this->photo = $photo;

		return $this;
	}


	public function getPresentation(): ?string
	{
		return $this->presentation;
	}

	public function setPresentation(?string $presentation): self
	{
		$this->presentation = $presentation;

		return $this;
	}

	/**
	 * @return Collection|Langue[]
	 */
	public function getLangues()
	{
		$str_langue = [];
		foreach ($this->langues as $langue) {
			$str_langue[] = $langue->getIntitule();
		}
		return $str_langue;
	}

	public function addLangue(Langue $langue): self
	{
		if (!$this->langues->contains($langue)) {
			$this->langues[] = $langue;
		}

		return $this;
	}

	public function removeLangue(Langue $langue): self
	{
		if ($this->langues->contains($langue)) {
			$this->langues->removeElement($langue);
		}

		return $this;
	}

	/**
	 * @return Collection|Disponibilite[]
	 */
	public function getDisponibilite(): Collection
	{
		return $this->disponibilite;
	}

	public function addDisponibilite(Disponibilite $disponibilite): self
	{
		if (!$this->disponibilite->contains($disponibilite)) {
			$this->disponibilite[] = $disponibilite;
			$disponibilite->setNounou($this);
		}

		return $this;
	}

	public function removeDisponibilite(Disponibilite $disponibilite): self
	{
		if ($this->disponibilite->contains($disponibilite)) {
			$this->disponibilite->removeElement($disponibilite);
			// set the owning side to null (unless already changed)
			if ($disponibilite->getNounou() === $this) {
				$disponibilite->setNounou(null);
			}
		}

		return $this;
	}

	public function getTableauDeDonnees() : array{
		$jsonUtilisateur = $this->getUtilisateur()->getTableauDeDonnees();
		$jsonNounou = array(
			"id" => $this->id,
			"nom" => $this->getUtilisateur()->getNom(),
			"prenom" => $this->getUtilisateur()->getPrenom(),
			"portable" => $this->getPortable(),
			"photo" => $this->getPhoto(),
			"date_naissance" => $this->getDatenaissance(),
			"presentation" => $this->getPresentation(),
			"langue" => $this->getLangues()
		);

		return array_merge($jsonUtilisateur, $jsonNounou);
	}

	public function getValide(): ?bool
	{
		return $this->valide;
	}

	public function setValide(bool $valide): self
	{
		$this->valide = $valide;
		return $this;
	}
	public function getDatenaissance(): ?\DateTimeInterface
	{
		return $this->datenaissance;
	}
	public function setDatenaissance(\DateTimeInterface $datenaissance): self
	{
		$this->datenaissance = $datenaissance;
		return $this;
	}

	public function getDispoJSON() : string {
		$json = [];
		foreach($this->disponibilite as $key => $dispo) {
			$json[$key] = $dispo->getTableauDeDonnees();
		}
		return json_encode($json);
	}
}
