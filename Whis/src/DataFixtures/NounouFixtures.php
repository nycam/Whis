<?php
namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\Utilisateur;
use App\Entity\Role;
use App\Entity\Nounou;
use App\Entity\Langue;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\RoleFixtures;
use App\Entity\Disponibilite;
use App\DataFixtures\LanguesFixtures;
use Doctrine\ORM\EntityManagerInterface;

class NounouFixtures extends Fixture implements DependentFixtureInterface
{
	private $repo;
	private $LANGUES_CHOIX;
	private $VILLES;

	public function __construct(EntityManagerInterface $doctrine) {
		$this->repo = $doctrine->getRepository(Langue::class);
		$this->LANGUES_CHOIX = [];
		$this->VILLES = ["Dijon", "Troyes", "Montpellier"];
		$toutes_les_langues = $this->repo->findAll();
		foreach($toutes_les_langues as $une_langue) {
			$this->LANGUES_CHOIX[] = $une_langue->getIntitule();
		}
	}
	public function load(ObjectManager $manager)
	{
		$role = $this->getReference("ROLE_NOUNOU");
		for($i = 0; $i<100; $i++) {
			$langueNounou = $this->repo->findOneBy([
				"intitule" => $this->LANGUES_CHOIX[rand(0, count($this->LANGUES_CHOIX)-1)]
			]);
			$villeNounou = $this->VILLES[rand(0, count($this->VILLES)-1)];
			$utilisateur = new Utilisateur();
			$password = "azerty";
			$hash = password_hash($password, PASSWORD_BCRYPT);
			$utilisateur->setPrenom("Nounou-$i")
				->setNom("Test")
				->setEmail("nounou$i@test.fr")
				->setPassword($hash)
				->setVille($villeNounou)
				->addRole($role);

			$nounou = new Nounou();

			$nounou->setUtilisateur($utilisateur)
				->setPortable("$i")
				->setPhoto("/uploads/nounous/alain.jpg")
				->setDatenaissance(new \DateTime('2016/01/01'))
				->setPresentation("oui")
				->setValide((rand(0,1)==0)?true:false)
				->addLangue($langueNounou)
				;
			$disponibilite = new Disponibilite();
			$disponibilite->setDatedebut( new \DateTime("2018/06/15") )
				->setDatefin( new \DateTime("2018/06/16") );
			$nounou->addDisponibilite($disponibilite);
			$manager->persist($disponibilite);
			$manager->persist($nounou);
		}
		$manager->flush();

	}

	public function getDependencies() {
		return array(RoleFixtures::class, LanguesFixtures::class);
	}
}
