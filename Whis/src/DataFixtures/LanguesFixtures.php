<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Langue;

class LanguesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

		$LANGUES = new \Doctrine\Common\Collections\ArrayCollection(["Français", "Anglais", "Allemand", "Espagnol", "Chinois", "Portugais"]);
		$this->setReference("LANGUES", $LANGUES);

		foreach($LANGUES as $value) {
			$langue = new Langue();
			$langue->setIntitule($value);
			$manager->persist($langue);
			$this->setReference($value, $langue);
		}



        $manager->flush();
    }
}
