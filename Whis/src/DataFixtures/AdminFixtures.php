<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Utilisateur;
use App\Entity\Role;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\RoleFixtures;

class AdminFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
		$role_admin = $this->getReference("ROLE_ADMIN");
		$motdepasse = "utt4ever";
		$hash = password_hash($motdepasse, PASSWORD_BCRYPT);
		$utilisateur = new Utilisateur();
		$utilisateur->setNom('root')
			->setPrenom('root')
			->setEmail('root@whis.fr')
			->setPassword($hash)
			->addRole($role_admin)
			->setVille('Troyes');
		$manager->persist($utilisateur);
        $manager->flush();
    }

	public function getDependencies() {
		return array(RoleFixtures::class);
	}
}
