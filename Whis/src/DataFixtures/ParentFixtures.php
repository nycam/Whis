<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Utilisateur;
use App\Entity\Role;
use App\Entity\Parents;
use App\Entity\Enfant;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\RoleFixtures;

class ParentFixtures extends Fixture implements DependentFixtureInterface
{
	public function load(ObjectManager $manager)
	{
		$utilisateur =new Utilisateur();
		$role = $this->getReference("ROLE_PARENT");
		$password = "123456";
		$hash = password_hash($password, PASSWORD_BCRYPT);
		$utilisateur->setPrenom("Parent")
			->setNom("Test")
			->setEmail("parent@test.fr")
			->setPassword($hash)
			->setVille("Troyes")
			->addRole($role);

		$parent = new Parents();
		$parent->setUtilisateur($utilisateur)
			->setInformation("Aucune info particulière, c'est juste un compte test mdr");

		$enf1 = new Enfant();
		$enf1->setPrenom("Marie-Carlin")
			->setDatenaissance(new \DateTime('2015/01/01'))
			->setParents($parent)
			->setRestrictionsAlimentaires("oui olala");

		$enf2 = new Enfant();
		$enf2->setPrenom("Jean-Roger")
			->setDatenaissance(new \DateTime('2015/01/02'))
			->setParents($parent)
			->setRestrictionsAlimentaires("non");

		$manager->persist($parent);
		$manager->persist($enf1);
		$manager->persist($enf2);
		$manager->flush();
	}

	public function getDependencies() {
		return array(RoleFixtures::class);
	}
}
