<?php

namespace App\Repository;

use App\Entity\Disponibilite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Langue;

/**
 * @method Disponibilite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Disponibilite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Disponibilite[]    findAll()
 * @method Disponibilite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DisponibiliteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Disponibilite::class);
    }

public function findAllByTownDateLangue(string $ville, \DateTime $dateDebut,\DateTime $dateFin, Langue $langue) : ?array {
		$ville = strToLower($ville);
		$query = $this->createQueryBuilder('d')
			->join("App\Entity\Nounou", "n", "WITH", "d.nounou = n.id")
			->join("App\Entity\Utilisateur", "u", "WITH", "n.utilisateur=u.id")
			->andWhere("?1 >= d.datedebut")
			->andWhere("?4 <= d.datefin")
			->andWhere("u.ville LIKE :ville")
			->andWhere(":langue MEMBER OF n.langues ")
			->andWhere("n.valide = true")
			->andWhere("d.parent is null")
			->setParameter("1", $dateDebut)
			->setParameter(":ville", "$ville%")
			->setParameter("langue", $langue)
			->setParameter("4", $dateFin)
			->getQuery();
		return $query->execute();
	}

	public function findAllByNounouAndAvailable(int $id) {
		$query = $this->createQueryBuilder("d")
			->andWhere("d.nounou_id = :id")
			->andWhere("d.parent_id != NULL")
			->setParameter("id", $id)
			->getQuery()
			;
		return $query->execute();
	}

	public function findAllDispoWithParent() {
		$conn = $this->getEntityManager()->getConnection();
		$sql = 'SELECT count(*) as nb_contrat
			FROM disponibilite
			WHERE MONTH(datedebut) = :date
			AND parent_id IS NOT NULL';
		$query = $conn->prepare($sql);
		$query->execute(["date" => date("m")]);
		return $query->fetchAll()[0];
	}

//    /**
//     * @return Disponibilite[] Returns an array of Disponibilite objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Disponibilite
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
