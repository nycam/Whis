<?php

namespace App\Repository;

use App\Entity\Nounou;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\Langue;

/**
 * @method Nounou|null find($id, $lockMode = null, $lockVersion = null)
 * @method Nounou|null findOneBy(array $criteria, array $orderBy = null)
 * @method Nounou[]    findAll()
 * @method Nounou[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class NounouRepository extends ServiceEntityRepository
{
	public function __construct(RegistryInterface $registry)
	{
		parent::__construct($registry, Nounou::class);
	}

	public function calculerMoyenneContrats(int $id_nounou) {
		$conn = $this->getEntityManager()->getConnection();
		$sql = 'SELECT avg(note) as note_moyenne
				FROM nounou n
				JOIN disponibilite d on n.id=d.nounou_id
				WHERE n.id=:idnounou';
		$query = $conn->prepare($sql);
		$query->execute(["idnounou" => $id_nounou]);
		//renvoyé sous forme de tableau
		//pour pouvoir être converti en json
		return $query->fetchAll()[0];
	}

	public function findAllNonValides() {
		$query = $this->createQueryBuilder("n")
			->andWhere("n.valide != true")
			->getQuery();
		return $query->execute();
	}

	public function findAllValides() {
		$query = $this->createQueryBuilder("n")
			->andWhere("n.valide = true")
			->getQuery();
		return $query->execute();
	}

}
