<?php

namespace App\Controller\ControllerVues;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ControleurConnexion extends Controller{
  /**
  * @Route("/vues/connexion", name="connexion")
  */
  public function routage(Request $request, AuthenticationUtils $authenticationUtils, AuthorizationCheckerInterface $auth){
    if($auth->isGranted('ROLE_ADMIN') || $auth->isGranted('ROLE_NOUNOU') || $auth->isGranted('ROLE_PARENT')){
      return $this->redirectToRoute('root');
    }else{
      //En cas d'erreur de connexion
      $error = $authenticationUtils->getLastAuthenticationError();

      //Dernier nom utilisateur entré
      $lastUsername = $authenticationUtils->getLastUsername();

      return $this->render('connexion/connexion.html.twig', array(
          'utilisateur' => null,
          'last_username' => $lastUsername,
          'error'         => $error
      ));
    }
    return $this->redirectToRoute('root');

  }

  /**
  * @Route("/logout", name="logout")
  */
  public function logout(){
    return $this->redirectToRoute('/');
  }
}
