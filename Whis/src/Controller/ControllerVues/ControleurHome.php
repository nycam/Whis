<?php

namespace App\Controller\ControllerVues;

use Symfony\Component\Debug\Debug;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Nounou;
use App\Entity\Parents;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use App\Entity\Langue;
use App\Entity\Utilisateur;

class ControleurHome extends Controller{

	/**
	 * @Route("/vue/accueil", name="accueil")
	 */
	public function AccueilUtilisateurConnecte(){
		//Si l'utilisateur est connecté
		$this->denyAccessUnlessGranted("IS_AUTHENTICATED_FULLY");
		$user = $this->getUser();
		$roles = $user->getRoles();
		$noteMoyenne =null;
		$image = null;
		$bloque = null;
		$langues=null;
		$villes=null;
		$nounou = $this->getDoctrine()
			->getRepository(Nounou::class)
			->findOneBy(['utilisateur'=>$user->getId()]);
		$parent = $this->getDoctrine()
			->getRepository(Parents::class)
			->findOneBy(["utilisateur"=>$user->getId()]);
		$url = "home/utilisateurNonConnecte.html.twig";
		if(in_array("ROLE_NOUNOU", $roles) && $nounou){
			$image = $nounou->getPhoto();
			$bloque = !$nounou->getValide();
			$url = "home/dashboardNounou.html.twig";
			$noteMoyenne = $this
				->getDoctrine()
				->getManager()
				->getRepository(Nounou::class)
				//le resultat de la moyenne est renvoyé sous forme de tableau, d'où les crochets
				->calculerMoyenneContrats($nounou->getId())["note_moyenne"];
		} else if(in_array("ROLE_PARENT", $roles)){
			$langues = $this
				->getDoctrine()
				->getRepository(Langue::class)
				->findAll()
				;
			$villes = $this
				->getDoctrine()
				->getRepository(Utilisateur::class)
				->findAllDistinctTown();
			$url = "home/dashboardParent.html.twig";
		} else if(in_array("ROLE_ADMIN", $roles)) {
			$url = "home/dashboardAdmin.html.twig";
		}
		return $this->render($url, [
			'utilisateur' => $user,
			'roles' => $roles,
			'image' => $image,
			'langues' => $langues,
			'bloque'=>$bloque,
			'nounou'=>$nounou,
			"noteMoyenne" => $noteMoyenne,
			'villes' => $villes,
			'dispos' => null
		]);
	}

	/**
	 *@Route("/", name="root")
	 */
	public function AccueilUtilisateurNonConnecte( AuthorizationCheckerInterface $auth) {
		if($auth->isGranted('ROLE_ADMIN') || $auth->isGranted('ROLE_NOUNOU') || $auth->isGranted('ROLE_PARENT')){
			return $this->redirectToRoute('accueil');
		}
		$langues = $this
			->getDoctrine()
			->getRepository(Langue::class)
			->findAll()
			;
		$villes = $this
			->getDoctrine()
			->getRepository(Utilisateur::class)
			->findAllDistinctTown();

		return $this->render('home/utilisateurNonConnecte.html.twig', [
			'utilisateur' => null,
			'nounous' => null,
			'villes' => $villes,
			'langues' => $langues,
			'dispos' => null
		]);
	}
}
