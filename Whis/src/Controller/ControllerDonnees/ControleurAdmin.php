<?php

namespace App\Controller\ControllerDonnees;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Nounou;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Disponibilite;

class ControleurAdmin extends Controller
{
	/**
	 * @Route("/traitement/admin/nounou/{id}/debloquer", name="debloquer_nounou")
	 */
	public function DebloquerNounou(Nounou $nounou)
	{
		$this->denyAccessUnlessGranted("ROLE_ADMIN");

		$nounou->setValide(true);

		$this
			->getDoctrine()
			->getManager()
			->flush();
		return new Response($nounou->getUtilisateur()->getNom()." a bien été débloquée.");
	}

	/**
	 * @Route("/traitement/admin/nounou/{id}/bloquer", name="bloquer_nounou")
	 */
	public function BloquerNounou(Nounou $nounou) {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");
		$nounou->setValide(false);

		$this
			->getDoctrine()
			->getManager()
			->flush();
		return new Response($nounou->getUtilisateur()->getNom()." a bien été bloquée.");
	}

	/**
	 * @Route("/donnees/admin/contrats/nombre", name="nombre_contrat")
	 */
	public function nombreDeContratMoisCourant() {
		$disponibilitePrises = $this
			->getDoctrine()
			->getManager()
			->getRepository(Disponibilite::class)
			->findAllDispoWithParent()
			;
		return JsonResponse::fromJsonString(json_encode($disponibilitePrises));
	}

	/**
	 * @Route("/donnees/admin/nounous/bloquees", name="nounous_bloquees")
	 */
	public function toutesLesNounouBloquee() {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");
		$nounousValide = $this
			->getDoctrine()
			->getManager()
			->getRepository(Nounou::class)
			->findAllNonValides();
		$json = [];
		foreach($nounousValide as $nounou) {
			$json[] = $nounou->getTableauDeDonnees();
		}
		$json = json_encode($json);
		return JsonResponse::fromJsonString($json);
	}

	/**
	 * @Route("donnees/admin/nounous", name="nounou_non_bloquees")
	 */
	public function toutesLesNounouNonBloquees() {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");
		$nounous = $this
			->getDoctrine()
			->getManager()
			->getRepository(Nounou::class)
			->findAllValides();
		$json = [];
		foreach($nounous as $nounou) {
			$json[]= $nounou->getTableauDeDonnees();
		}
		return JsonResponse::fromJsonString(json_encode($json));
	}

	/**
	 * @Route("traitement/admin/nounou/{id}/supprimer", name="admin_supprimer_nounou")
	 */
	public function supprimerNounou(Nounou $nounou) {
		$this->denyAccessUnlessGranted("ROLE_ADMIN");
		$manager =	$this
			->getDoctrine()
			->getManager();
		$manager->remove($nounou);
		$manager->flush();
		return new Response("nounou supprimée");
	}

}
