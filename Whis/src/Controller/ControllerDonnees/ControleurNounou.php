<?php

namespace App\Controller\ControllerDonnees;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Nounou;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Form\NounouFormType;
use Psr\Log\LoggerInterface;
use App\Service\MdpEncodeur;
use App\Service\NounouPicUploader;
use App\Entity\Disponibilite;
use App\Form\DisponibiliteFormType;
use App\Service\AttrapeurNounou;
use App\Entity\Langue;
use DateTime;
use App\Entity\Role;
use App\Entity\Utilisateur;


class ControleurNounou extends Controller{
	/**
	 * @Route("donnee/nounou/{id}", name="nounou_controller")
	 */
	public function routage(Nounou $nounou){
		$json = json_encode($nounou->getTableauDeDonnees());
		return JsonResponse::fromJsonString($json);
	}

	/**
	 * @Route("donnees/nounous", name="toutes_les_nounous")
	 */
	public function obtenirToutesLesNounous() {
		$nounous = $this
			->getDoctrine()
			->getRepository(Nounou::class)
			->findAll();
		return $this->render('nounou/liste.html.twig', ["nounous" => $nounous]);
	}

	/**
	 * @Route("donnees/nounou/moyenne", name="moyenne_nounou")
	 */
	public function obtenirMoyenne() {
		$this->denyAccessUnlessGranted("ROLE_NOUNOU");
		$id = $this->getUser()->getNounou()->getId();
		$moyenne = $this
					->getDoctrine()
					->getManager()
					->getRepository(Nounou::class)
					->calculerMoyenneContrats($id);
		return JsonResponse::fromJsonString(json_encode($moyenne));
	}

	/**
	 *@Route("donnees/nounous/creneaux", name="creneaux_nounou")
	 */
	public function obtenirDispoNounou() {
		$this->denyAccessUnlessGranted("ROLE_NOUNOU");
		$nounou = $this->getUser()->getNounou();
		return JsonResponse::fromJsonString($nounou->getDispoJSON());
	}



	/**
	 * @Route("traitement/nounou/creneau/ajouter", name="ajouter_creneau")
	 */
	public function ajouterCreneau(Request $r, AttrapeurNounou $attrapeur) {
		$this->denyAccessUnlessGranted('ROLE_NOUNOU');
		$nounou = $attrapeur->obtenirNounou(
				$this->getUser()
				->getId()
			);
		$dispo = new Disponibilite();
		$dispo->setDatedebut(new \DateTime($r->get("dateDebut")))
			->setDatefin(new \DateTime($r->get("dateFin")))
			->setNounou($nounou)
			->setAccepte(false);
		$manager = $this->getDoctrine()->getManager();
		$nounou->addDisponibilite($dispo);
		$manager->persist($dispo);
		$manager->flush();
		return JSonResponse::fromJsonString("{}");
	}

	/**
	 * @Route("traitement/inscription/nounou/", name="inscription_nounou")
	 */
	public function inscription(Request $r, MdpEncodeur $encodeur, NounouPicUploader $upload, AuthorizationCheckerInterface $auth) {
		if($auth->isGranted('ROLE_NOUNOU')){
			return $this->redirectToRoute('Accueil');
		}

		$nounou = new Nounou();
		$form = $this->createForm(NounouFormType::class, $nounou, array(
			'entity_manager' => $this->getDoctrine()->getManager()
		));

		$form->handleRequest($r);

		if($form->isSubmitted() && $form->isValid() )  {
			//Encoder le mot de passe avec bcrypt
			$mdp = $encodeur->encoder($nounou->getUtilisateur());
			$nounou->getUtilisateur()->setPassword($mdp);
			$manager = $this->getDoctrine()->getManager();

			//uploader la photo
			$uriPhoto = $upload->televerser($nounou->getPhoto());
			$nounou->setPhoto($uriPhoto);

			//Attribuer le role de nounou
			$role = $this
				->getDoctrine()
				->getRepository(Role::class)
				->findOneBy(["intitule" => "ROLE_NOUNOU"])
				;

			$nounou->getUtilisateur()->addRole($role);

			$manager->persist($nounou);
			$manager->flush();
			return $this->redirectToRoute('accueil');
		}
		return $this->render('inscription/inscriptionNounou.html.twig', array(
			'form' => $form->createView(),
			'utilisateur' => null
		));
	}

	/**
	 * @Route("traitement/disponibilite/{id}/supprimer", name="supprimer_creneau")
	 */
	public function supprimerCreneau(Disponibilite $dispo)
	{
		$this->denyAccessUnlessGranted("ROLE_NOUNOU");
		$utilisateur = $this->getUser();
		if ( $dispo->getProprietaire()->getUtilisateur()->getId() != $utilisateur->getId() )	{
			throw new UnauthorizedHttpException("Cette disponibilité ne vous appartient pas.");
		}
		$this
			->getDoctrine()
			->getManager()
			->remove($dispo);
		$this
			->getDoctrine()
			->getManager()
			->flush();
		return $this->redirectToRoute("accueil");
	}

	/**
	 * @Route("traitement/recherche/nounou", name="recherche_nounou")
	 */
	public function recherche(Request $r) {
		$villes = $this
					->getDoctrine()
					->getManager()
					->getRepository(Utilisateur::class)
					->findAllDistinctTown()
					;
		$langues = $this
					->getDoctrine()
					->getManager()
					->getRepository(Langue::class)
					->findAll()
					;
		$user = $this->getUser();
		$roles = $user->getRoles();
		$repo = $this
			->getDoctrine()
			->getManager()
			->getRepository(Disponibilite::class);
		$langue = $this->getDoctrine()->getRepository(Langue::class)->findOneBy(["intitule" => $r->query->get("langue", "Français")]);
		$ville = $r->query->get("ville", "");
		$dateDebut = $r->query->get("dateDebut", "")." ".$r->query->get("timeDebut","");
		$dateFin = $r->query->get("dateFin", "")." ".$r->query->get("timeFin","");
		$dateDebut = new \DateTime($dateDebut);
		$dateFin = new \DateTime($dateFin);
		$dispo = $repo->findAllByTownDateLangue($ville, $dateDebut, $dateFin, $langue);

		return $this->render('home/utilisateurNonConnecte.html.twig', [
			"dispos" => $dispo,
			"utilisateur" => $user,
			'roles' => $roles,
			"langues" => $langues,
			"villes" => $villes
		]);
	}

	/**
	 * @Route("vue/parametres/nounou", name="param_nounou")
	 */
	public function parametre(Request $r, MdpEncodeur $encoder){
		$this->denyAccessUnlessGranted("ROLE_NOUNOU");
		$nounou = $this->getUser()->getNounou();
		$test = new Nounou();
		$form = $this->createForm(NounouFormType::class, $nounou, array(
			'entity_manager' => $this->getDoctrine()->getManager()
		));
		$form->handleRequest($r);
		if($form->isSubmitted() && $form->isValid() )  {
			$mdp = $encoder->encoder($parent->getUtilisateur());
			$nounou->getUtilisateur()->setPassword($mdp);

			//Insertion en base de l'objet créé
			$manager = $this->getDoctrine()->getManager();
			$manager->flush();

			return $this->redirectToRoute('accueil');
		}
		return $this->render('home/parametreNounou.html.twig', array(
			'form' => $form->createView(),
			'roles' => $nounou->getUtilisateur()->getRoles(),
			'image' =>$nounou->getPhoto(),
			'utilisateur' => $nounou->getUtilisateur()
		));
		return $this->redirectToRoute('accueil');
	}
}
