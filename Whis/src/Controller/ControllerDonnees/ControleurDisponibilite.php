<?php

namespace App\Controller\ControllerDonnees;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Disponibilite;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ControleurDisponibilite extends Controller
{
    /**
     * @Route("/disponibilite", name="disponibilite")
     */
    public function index()
    {
        return $this->render('disponibilite/index.html.twig', [
            'controller_name' => 'DisponibiliteController',
        ]);
    }

	/**
	 * @Route("/traitement/disponibilite/{id}/refuser", name="refuser_disponibilite")
	 */
	public function refuserDisponibilite(Disponibilite $dispo) {
		$this->denyAccessUnlessGranted("ROLE_NOUNOU");
		$nounou = $this->getUser()->getNounou();
		if($nounou->getId() != $dispo->getNounou()->getId()) {
			throw new UnauthorizedHttpException("Ce créneau ne vous appartient pas.");
		}

		$dispo->setParent(null);
		$this
			->getDoctrine()
			->getManager()
			->flush();
		return new Response("ok");
	}

	/**
	 * @Route("/traitement/disponibilite/{id}/reserver", name="reserver_dispo")
	 */
	public function reserverDisponibilite(Disponibilite $dispo) {
		$this->denyAccessUnlessGranted("ROLE_PARENT");
		$parent = $this->getUser()->getParent();
		if($dispo->getParent() != null){
			throw new UnauthorizedHttpException("Le créneau est déjà réservé");
		}
		$parent->addCreneauxReserves($dispo);
		$this
			->getDoctrine()
			->getManager()
			->flush()
		;
		return new Response("le creneau a bien été réservé");
	}

	/**
	 * @Route("/traitement/disponibilite/{id}/valider", name="valider_dispo")
	 */
	public function validerDisponibilite(Disponibilite $dispo) {
		$this->denyAccessUnlessGranted("ROLE_NOUNOU");
		$nounou = $this->getUser()->getNounou();
		if($nounou->getId() != $dispo->getNounou()->getId()) {
			throw new UnauthorizedHttpException("Ce créneau ne vous appartient pas.");
		}
		$dispo->setAccepte(true);
		$this
			->getDoctrine()
			->getManager()
			->flush();
		return new Response("ok");
	}

	/**
	 *@Route("traitement/disponibilite/{id}/noter", name="noter_dispo")
	 */
	public function noterDispo(Request $request, Disponibilite $dispo){
		$this->denyAccessUnlessGranted("ROLE_PARENT");
		$parent = $this->getUser()->getParent();
		if ($parent->getId() != $dispo->getParent()->getId()) {
			throw new UnauthorizedHttpException("Ce créneau ne vous appartient pas.");
		}

		if($dispo->getDatefin() > new \DateTime(date("Y/m/d"))) {
			throw new UnauthorizedHttpException("Le creneau n'est pas encore passé, vous ne pouvez pas encore le noter");
		}
		$note = $request->query->get("note", 0);
		$dispo->setNote($note);
		$this->getDoctrine()
			->getManager()
			->flush();
		return new Response("La note a bien été prise en compte");
	}

	/**
	 * @Route("traitement/disponibilite/{id}/abandonner")
	 */
	public function abandonnerDisponibilite(Disponibilite $dispo) {
		$this->denyAccessUnlessGranted("ROLE_PARENT");
		$parent = $this->getUser()->getParent();
		if ($dispo->getParent()->getId() != $parent->getId() )
		{
			throw new UnauthorizedHttpException("Vous n'êtes pas le parent qui a réservé ce créneau");
		}

		$parent->removeCreneauxReserves($dispo);
		$this
			->getDoctrine()
			->getManager()
			->flush();
		return new Response("Le creneau a bien été abandonné");
	}
}
