<?php

namespace App\Controller\ControllerDonnees;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Enfant;

class ControleurEnfant extends Controller
{
	/**
	 * @Route("traitement/enfant/{id}/supprimer", name="supprimer_enfant")
	 */
    public function supprimerEnfant(Enfant $enfant)
    {
		$this->denyAccessUnlessGranted("ROLE_PARENT");
		$parent = $this->getUser()->getParent();
		if($enfant->getParents()->getUtilisateur()->getId() !== $parent->getUtilisateur()->getId()){
      //Je commente juste parce que ta phrase est drole. oui.
			throw new UnauthorizedHttpException("Cet enfant ne vous appartient pas :/");
		}
		$manager = $this->getDoctrine()->getManager();
		$manager->remove($enfant);
		$manager->flush();
		return $this->redirectToRoute("accueil");
    }
}
