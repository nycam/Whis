<?php

namespace App\Controller\ControllerDonnees;

use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Parents;
use App\Entity\Utilisateur;
use App\Entity\Enfant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\MdpEncodeur;
use Symfony\Component\HttpFoundation\Request;
use App\Form\ParentFormType;
use App\Form\EnfantFormType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use App\Entity\Role;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ControleurParent extends Controller{

	/**
	*
	* @Route("donnees/parents/enfants/{id}", name="Recup_Enfant")
	*/
	public function recupEnfants(Utilisateur $user){
		$this->denyAccessUnlessGranted("ROLE_PARENT");
		$parent = $this->getUser()->getParent();
		$enfants = $parent->getEnfants();
		$enfants_trouves = [];
		foreach ($enfants as $enfant) {
			array_push($enfants_trouves, $enfant->getDatas());
		}
		$json = json_encode($enfants_trouves);
		return JsonResponse::fromJsonString($json);
	}
	/**
	 * Ajout d'un enfant a un parent spécifique
	 * @Route("traitement/parent/enfant/ajouter", name="ajouter_enfant")
	 */
	public function ajouterEnfant(Request $r) {

		$this->denyAccessUnlessGranted("ROLE_PARENT");
		$parent = $this->getUser()->getParent();

		$prenom = $r->get("prenom", "");
		$naissance = $r->get("naissance", "");
		$restriction = $r->get("restriction", "");

		if (
			$prenom != ""
			&&
			$naissance != ""
		) {
			$manager = $this->getDoctrine()->getManager();
			$naissance = new \DateTime($naissance);
			$enfant = new Enfant();
			$enfant->setPrenom($prenom)
				->setDatenaissance($naissance)
				->setRestrictionsAlimentaires($restriction)
			;
			$manager->persist($enfant);
			$parent->addEnfant($enfant);
			$manager->flush();
		}
		return new Response("salut : $prenom");
	}

	/**
	 * Page d'inscription pour un parent
	 * @Route("traitement/inscription/parent/", name="inscription_parent")
	 */
	public function inscription(Request $r, MdpEncodeur $encoder) {

		//Création du parent et de son formulaire spécifique
		$parent = new Parents();
		$form = $this->createForm(ParentFormType::class, $parent);
		$form->handleRequest($r);

		//Vérification du formulaire et attribution des données a l'objet parent
		if($form->isSubmitted() && $form->isValid() )  {
			$mdp = $encoder->encoder($parent->getUtilisateur());
			$parent->getUtilisateur()->setPassword($mdp);
			$role = $this->getDoctrine()
						->getRepository(Role::Class)
						->findOneBy([
							"intitule" => "ROLE_PARENT"
						]);
			$parent->getUtilisateur()->addRole($role);

			//Insertion en base de l'objet créé
			$manager = $this->getDoctrine()->getManager();
			$manager->persist($parent);
			$manager->flush();

			return $this->redirectToRoute('accueil');
		}

		return $this->render('inscription/inscriptionParent.html.twig', array(
			'form' => $form->createView(),
			'utilisateur' => null
		));

	}

	/**
	* @Route("vue/parametres/parent", name="param_parent")
	*/
	public function parametre(Request $r, MdpEncodeur $encoder){
		$this->denyAccessUnlessGranted("IS_AUTHENTICATED_FULLY");
		$user = $this->getUser();
		$parent = $this->getDoctrine()
			->getRepository(Parents::class)
			->findOneBy(["utilisateur"=>$user->getId()]);
		if($parent){
			$form = $this->createForm(ParentFormType::class, $parent);
			$form->handleRequest($r);
			if($form->isSubmitted() && $form->isValid() )  {
				$mdp = $encoder->encoder($parent->getUtilisateur());
				$parent->getUtilisateur()->setPassword($mdp);

				//Insertion en base de l'objet créé
				$manager = $this->getDoctrine()->getManager();
				$manager->flush();

				return $this->redirectToRoute('accueil');
			}
			return $this->render('home/parametreParents.html.twig', array(
				'form' => $form->createView(),
				'roles' => $user->getRoles(),
				'utilisateur' => $user
			));
		}
		return $this->redirectToRoute('accueil');
	}
}
