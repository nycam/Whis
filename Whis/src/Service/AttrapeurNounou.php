<?php

namespace App\Service;

use App\Entity\Nounou;
use Doctrine\ORM\EntityManagerInterface;

class AttrapeurNounou {

	private $nounous;

	public function __construct(EntityManagerInterface $doctrine) {
		$this->nounous = $doctrine->getRepository(Nounou::class);
	}

	public function obtenirNounou(int $id) : Nounou {
		return $this->nounous->findOneBy(["utilisateur" => $id]);
	}
	

}
