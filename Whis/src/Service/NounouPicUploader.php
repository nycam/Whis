<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class NounouPicUploader {

	private $nounouRep;
	private $appPath;

	public function __construct(string $repertoire, string $appPath) {
		$this->nounouRep = $repertoire;
		$this->appPath = $appPath;
	}

	public function televerser(UploadedFile $fichier) : string {
		$nom = md5(uniqid($fichier)).'.'.$fichier->guessClientExtension();
		$fichier->move($this->nounouRep, $nom);
		$nom = "/uploads/nounous/".$nom;
		return $nom;
	}

	public function getNounouRep() : string
	{
		return $this->nounouRep;
	}
}
?>
