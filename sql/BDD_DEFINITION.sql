drop schema if exists Whis;

create schema Whis;
use Whis;

CREATE TABLE `langue` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `intitule` VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE `role` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `intitule` VARCHAR(30) NOT NULL
);

CREATE TABLE `utilisateur` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `nom` VARCHAR(255) NOT NULL,
  `prenom` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) UNIQUE NOT NULL,
  `ville` VARCHAR(255) NOT NULL,
  `mdphash` VARCHAR(255) NOT NULL
);

CREATE TABLE `nounou` (
  `portable` VARCHAR(255) UNIQUE NOT NULL,
  `photo` VARCHAR(255) NOT NULL,
  `datenaissance` DATE NOT NULL,
  `presentation` VARCHAR(255) NOT NULL,
  `utilisateur` INTEGER  PRIMARY KEY
);

ALTER TABLE `nounou` ADD CONSTRAINT `fk_nounou__utilisateur` FOREIGN KEY (`utilisateur`) REFERENCES `utilisateur` (`id`);

CREATE TABLE `disponibilite` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `datedebut` DATETIME,
  `datefin` DATETIME NOT NULL,
  `nounou` INTEGER NOT NULL
);

CREATE INDEX `idx_disponibilite__nounou` ON `disponibilite` (`nounou`);

ALTER TABLE `disponibilite` ADD CONSTRAINT `fk_disponibilite__nounou` FOREIGN KEY (`nounou`) REFERENCES `nounou` (`utilisateur`);

CREATE TABLE `experience` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `nounou` INTEGER NOT NULL,
  `experience` VARCHAR(140) NOT NULL
);

CREATE INDEX `idx_experience__nounou` ON `experience` (`nounou`);

ALTER TABLE `experience` ADD CONSTRAINT `fk_experience__nounou` FOREIGN KEY (`nounou`) REFERENCES `nounou` (`utilisateur`);

CREATE TABLE `langue_nounous` (
  `nounou` INTEGER NOT NULL,
  `langue` INTEGER NOT NULL,
  CONSTRAINT `pk_langue_nounous` PRIMARY KEY (`nounou`, `langue`)
);

CREATE INDEX `idx_langue_nounous` ON `langue_nounous` (`langue`);

ALTER TABLE `langue_nounous` ADD CONSTRAINT `fk_langue_nounous__langue` FOREIGN KEY (`langue`) REFERENCES `langue` (`id`);

ALTER TABLE `langue_nounous` ADD CONSTRAINT `fk_langue_nounous__nounou` FOREIGN KEY (`nounou`) REFERENCES `nounou` (`utilisateur`);

CREATE TABLE `parents` (
  `information` VARCHAR(255) NOT NULL,
  `utilisateur` INTEGER  PRIMARY KEY
);

ALTER TABLE `parents` ADD CONSTRAINT `fk_parents__utilisateur` FOREIGN KEY (`utilisateur`) REFERENCES `utilisateur` (`id`);

CREATE TABLE `enfant` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `prenom` VARCHAR(255) NOT NULL,
  `datenaissance` DATE NOT NULL,
  `parents` INTEGER NOT NULL
);

CREATE INDEX `idx_enfant__parents` ON `enfant` (`parents`);

ALTER TABLE `enfant` ADD CONSTRAINT `fk_enfant__parents` FOREIGN KEY (`parents`) REFERENCES `parents` (`utilisateur`);

CREATE TABLE `propositionlangue` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `intitule` VARCHAR(255) NOT NULL,
  `nounou` INTEGER NOT NULL
);

CREATE INDEX `idx_propositionlangue__nounou` ON `propositionlangue` (`nounou`);

ALTER TABLE `propositionlangue` ADD CONSTRAINT `fk_propositionlangue__nounou` FOREIGN KEY (`nounou`) REFERENCES `nounou` (`utilisateur`);

CREATE TABLE `reservation` (
  `id` INTEGER PRIMARY KEY AUTO_INCREMENT,
  `enfant` INTEGER NOT NULL,
  `disponibilite` INTEGER NOT NULL,
  `langue` INTEGER NOT NULL,
  `note` VARCHAR(255)
);

CREATE INDEX `idx_reservation__disponibilite` ON `reservation` (`disponibilite`);

CREATE INDEX `idx_reservation__enfant` ON `reservation` (`enfant`);

CREATE INDEX `idx_reservation__langue` ON `reservation` (`langue`);

ALTER TABLE `reservation` ADD CONSTRAINT `fk_reservation__disponibilite` FOREIGN KEY (`disponibilite`) REFERENCES `disponibilite` (`id`);

ALTER TABLE `reservation` ADD CONSTRAINT `fk_reservation__enfant` FOREIGN KEY (`enfant`) REFERENCES `enfant` (`id`);

ALTER TABLE `reservation` ADD CONSTRAINT `fk_reservation__langue` FOREIGN KEY (`langue`) REFERENCES `langue` (`id`);

CREATE TABLE `role_utilisateurs` (
  `utilisateur` INTEGER NOT NULL,
  `role` INTEGER NOT NULL,
  CONSTRAINT `pk_role_utilisateurs` PRIMARY KEY (`utilisateur`, `role`)
);

CREATE INDEX `idx_role_utilisateurs` ON `role_utilisateurs` (`role`);

ALTER TABLE `role_utilisateurs` ADD CONSTRAINT `fk_role_utilisateurs__role` FOREIGN KEY (`role`) REFERENCES `role` (`id`);

ALTER TABLE `role_utilisateurs` ADD CONSTRAINT `fk_role_utilisateurs__utilisateur` FOREIGN KEY (`utilisateur`) REFERENCES `utilisateur` (`id`)
