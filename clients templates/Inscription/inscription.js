var nbLangue = 1;
function previewFile(){
       var preview = document.querySelector('img'); //selects the query named img
       var file    = document.querySelector('input[type=file]').files[0]; //sames as here
       var reader  = new FileReader();

       reader.onloadend = function () {
           preview.src = reader.result;
       }

       if (file) {
           reader.readAsDataURL(file); //reads the data as a URL
       } else {
           preview.src = "";
       }
  }

function ajouterLangue(){
  $('#langues').append("\
  <div class='row ligne' id='langues"+nbLangue+"'>\
  <select id='langue"+nbLangue+"' class='col-sm-5 col-sm-offset-1'>\
    <option value='Francais'>Francais</option>\
    <option value='Anglais'>Anglais</option>\
    <option value='Autre'>Autre</option>\
  </select>\
  <input type='hidden' id='Autre-Langue"+nbLangue+"' name='Autre-Langue"+nbLangue+"' value=''>\
  ");
  $('#langue'+nbLangue).on("change", {langue: nbLangue}, afficherAutre);
  if(nbLangue == 1){
    $("#supp-langue").on('click', supprimerLangue);
    $("#supp-langue").prop("disabled", false);
  }
  nbLangue++;
}

function supprimerLangue(){
  if(nbLangue > 1){
    console.log("langues"+(nbLangue-1));
    $("#langues"+(nbLangue-1)).remove();
    nbLangue--;
    if(nbLangue == 1){
      $("#supp-langue").off('click');
      $("#supp-langue").prop("disabled",true);
    }
  }
}

function afficherAutre(event){
  var numlangue = String(event.data.langue);
  console.log("Triggered : langue "+ numlangue);
  if($('#langue'+numlangue).val() == "Autre"){
    $("#Autre-Langue"+numlangue).attr("type", "text");
  }else{
    $("#Autre-Langue"+numlangue).attr("type", "hidden");
  }
}

$(function() {
  $('#datetimepicker1').datetimepicker({
    format: "DD/MM/YYYY"
  });
  $('#upload').on('change', previewFile);
  $('#ajout-langue').on('click', ajouterLangue);
  $('#langue0').on('change', {langue: '0'},afficherAutre);
});
